Project Report: Configuration Management with Puppet and Ansible
Introduction
This report summarizes our work on automating system configurations using Puppet and Ansible. We organized our project using GitLab at Université Lyon 1 for collaboration and version control.

**Part 1: Git Management**

We structured our Git repository with several branches:
main: Contains the final code and a detailed README.md.
ssh_puppet: Development of Puppet modules for SSH.
ssh_puppet_production: Production-ready Puppet modules for SSH.
ssh_ansible: Development of Ansible playbooks for SSH.
ssh_ansible_production: Production-ready Ansible playbooks for SSH.
We used tags to mark key development milestones.

**Part 2: Puppet Modules and Ansible Playbooks**

Puppet Modules:
We created two Puppet modules to install and configure SSH and Apache on Ubuntu and CentOS. Each module is organized with directories such as manifests, files, and templates, containing files like init.pp, params.pp, install.pp, config.pp, and service.pp. We documented each class and used conditionals to ensure compatibility across different operating systems.

Ansible Playbooks:
Ansible playbooks are written in YAML to automate the installation and configuration of SSH and Apache. Each playbook includes detailed tasks to install packages, manage services, and configure files. We utilized variables for customization, such as SSH port and protocol.

**Part 3: Puppet Environments and Ansible Inventories**

Puppet Environments:
We configured three environments: production, development, and test, each with its own branches in Git. Each environment uses separate manifests and configuration files to enable separate deployment and testing of Puppet configurations.

Ansible Inventories:
An Ansible inventory was used to manage hosts across development and production environments. This centralized management facilitated targeted execution of playbooks.

**Part 4: Tool Comparison**

We compared Puppet and Ansible based on several criteria:
Ease of Learning and Use: Ansible is easier to learn with its intuitive YAML syntax compared to Puppet manifests.
Configuration Management: Puppet uses Hiera for separating data from code, while Ansible uses inventories to organize hosts.
Flexibility and Modularity: Puppet excels in complex system configurations, while Ansible is versatile for operational automation.
Specific Use Cases: Puppet is often used for large-scale complex environments, whereas Ansible is ideal for IT automation tasks.
Conclusion
This project demonstrated our capability to effectively use Puppet and Ansible for automating system configurations. We adopted best practices in code management with Git, ensuring structured development, testing, and deployment. Each tool was leveraged according to its strengths, contributing to efficient and automated management of our infrastructures.