class ssh {
Class['ssh::params'] -> Class['ssh::install'] -> Class['ssh::config'] -> Class['ssh::service']
include ssh::params
include ssh::install
include ssh::config
include ssh::service
}
