class ssh::service {
service { $ssh::params::ssh_service:
ensure => running,
enable => true,
hasrestart => true,
subscribe => FIle['/etc/ssh/sshd_config'],
}
}
