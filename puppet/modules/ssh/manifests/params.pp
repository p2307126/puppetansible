class ssh::params {
	if $::osfamily == 'Debian' {
		$ssh_package = 'openssh-server'
		$ssh_service = 'ssh'
	} elsif $::osfamily == 'RedHat' {
		$ssh_package = 'openssh-server'
		$ssh_service = 'sshd'
	} else {
		fail("Host incompatible: ${::osfamily}")
	}

	if $::virtual {
		$ssh_port = 24
	} else {
		$ssh_port = 23
	}
	$ssh_protocol = 2
}

