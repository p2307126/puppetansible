class ssh::install {
	package { $ssh::params::ssh_package:
		ensure => installed,
	}
}
