class ssh::config {
	file { '/etc/ssh/sshd_config':
		ensure => file,
		content => template('/home/lmartin/ssh/puppet/modules/ssh/templates/sshd_config.erb'),
		owner => 'root',
		group => 'root',
		mode => '0600',
		require => Package[$ssh::params::ssh_package],
		notify => Service[$ssh::params::ssh_service],
	}
}
