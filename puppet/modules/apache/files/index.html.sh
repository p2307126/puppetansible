#!/bin/bash

# Générer la page index.html avec les informations demandées
cat <<EOF > /var/www/html/index.html
<!DOCTYPE html>
<html>
<head>
  <title>System Information</title>
</head>
<body>
  <h1>System Information</h1>
  <p>Temps depuis démarrage : $(uptime -p)</p>
  <p>Date et heure actuelle : $(date "+%d-%m-%Y %H:%M:%S")</p>
  <p>Système d'exploitation : $(uname -s)</p>
  <p>Version du noyau : $(uname -r)</p>
</body>
</html>
EOF