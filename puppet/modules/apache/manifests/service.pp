class apache::service {
  service { $apache::params::service_name:
    ensure    => running,
    enable    => true,
    require   => Package[$apache::params::package_name],
  }
}