class apache::params {
  $osfamily = $facts['os']['family']

  $package_name = $osfamily ? {
    'Debian' => 'apache2',
    'RedHat' => 'httpd',
    default  => 'apache2',
  }
  $service_name = $osfamily ? {
    'Debian' => 'apache2',
    'RedHat' => 'httpd',
    default  => 'apache2',
  }
  $config_file = $osfamily ? {
    'Debian' => '/etc/apache2/apache2.conf',
    'RedHat' => '/etc/httpd/conf/httpd.conf',
    default  => '/etc/apache2/apache2.conf',
  }
}