class apache::config {
  file { $apache::params::config_file:
    ensure  => file,
    content => template('apache/apache2.conf.erb'),
    require => Package[$apache::params::package_name],
    notify  => Service[$apache::params::service_name],
  }
  file { '/usr/local/bin/index.html.sh':
    ensure  => file,
    mode    => '0755',
    source  => 'puppet:///modules/apache/index.html.sh',
    require => Package[$apache::params::package_name],
  }

  exec { 'index.html':
    command     => '/usr/local/bin/index.html.sh',
    refreshonly => true,
    subscribe   => File['/usr/local/bin/index.html.sh'],
  }
}