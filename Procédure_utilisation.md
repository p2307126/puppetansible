*Prerequisites:*

Ensure Puppet is installed on your system.
Clone the Git repository containing Puppet modules:
bash
Copier le code
git clone <repository_url>
Installing and Configuring SSH with Puppet:
Navigate to Puppet Module Directory:

bash
Copier le code
cd puppet/modules/ssh

Edit Parameters (Optional):

Adjust parameters in params.pp if needed, such as SSH port and protocol.
Apply Puppet Manifest:
Apply the Puppet manifest to install SSH:

bash
Copier le code
sudo puppet apply --modulepath=/path/to/puppet/modules -e "include ssh"
Replace /path/to/puppet/modules with the actual path where your Puppet modules are located.

Verify SSH Installation:

Check if SSH is installed and configured as expected on the target system.

Installing and Configuring Apache with Puppet:
Navigate to Puppet Module Directory:

bash
Copier le code
cd puppet/modules/apache
Edit Parameters (Optional):

Adjust parameters in params.pp if needed, such as Apache configurations.

Apply Puppet Manifest:
Apply the Puppet manifest to install Apache:

bash
Copier le code
sudo puppet apply --modulepath=/path/to/puppet/modules -e "include apache"
Replace /path/to/puppet/modules with the actual path where your Puppet modules are located.

Verify Apache Installation:

Check if Apache is installed and configured correctly on the target system.

Using Ansible
Prerequisites:

Ensure Ansible is installed on your system.
Clone the Git repository containing Ansible playbooks:
bash
Copier le code
git clone <repository_url>
Installing and Configuring SSH with Ansible:
Navigate to Ansible Playbook Directory:

bash
Copier le code
cd ansible
Edit Ansible Playbook (ssh.yml):

Adjust the SSH configuration in ssh.yml as needed, including SSH port and protocol.

Execute Ansible Playbook:
Run the Ansible playbook to install and configure SSH:

bash
Copier le code
ansible-playbook ssh.yml
Verify SSH Installation:

Check if SSH is installed and configured as expected on the target system.

Installing and Configuring Apache with Ansible:
Edit Ansible Playbook (apache.yml):
Modify the Apache installation and configuration tasks in apache.yml.

Execute Ansible Playbook:
Run the Ansible playbook to install and configure Apache:

bash
Copier le code
ansible-playbook apache.yml
Verify Apache Installation:

Check if Apache is installed and configured correctly on the target system.